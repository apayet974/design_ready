import React, { Component } from 'react';
import Script from './component/view/Script.js';
import Header from './component/view/Header.js';
import Main from './component/view/Main.js';
import Footer from './component/view/Footer.js';
import * as head from './component/controller/headDom.js';

class App extends Component {
  
	render() {
		head.setStyle();
    		return (
	   		 <div>
	    			<div id="wrapper">
	    				<Header />
	    				<Main />
	    				<Footer />
	    			</div>
	    			<div id="bg"></div>
	    			<Script />
	   		 </div>
   		 );
  }
}

export default App;
