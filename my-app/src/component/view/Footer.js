import React, {Component} from 'react';
import {footer_content} from '../model/base.js';

export default class Footer extends Component
{
	render()
	{
		return(
			<footer id="footer">
			{
				footer_content.map(({id, copyright, a_link, a_content}) => (
					<p key={id} className="copyright">&copy; {copyright} <a href={a_link} >{a_content}</a>.</p>
				))
			}
			</footer>
		);
	}
}
